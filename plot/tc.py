#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

fig = plt.figure(figsize=(16, 12))

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

files = glob(f'{output}/*')
files.sort()
values = {}
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    g = info['parameters']['g']
    label = f't={t}\ng={g}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_real_data(f.read())
        beta = data[:, 0]
        lam = np.array(data[:, 1])
        dist = data[:, 2]
        # lam = lam[dist > 1e-7]
        # beta = beta[dist > 1e-7]
        # dist = dist[dist > 1e-7]
        if len(lam) > 1:
            try:
                # bc = (beta[-2] - beta[-1] + beta[-1]*lam[-2] - beta[-2]*lam[-1]) / (lam[-2] - lam[-1])
                bc = np.interp(1., lam, beta, None, None)
                if bc is not None and not np.isnan(bc) and bc < np.nanmax(beta) and bc > np.nanmin(beta):
                    if (t, g) in values.keys():
                        values[(t, g)][0] += bc
                        values[(t, g)][1] += 1
                    else:
                        values[(t, g)] = [bc, 1]
            except Exception as e:
                print(e)

data = {}
for ((t, g), (bc, n)) in values.items():
    data.setdefault(t, []).append([g, (n/bc)/g**2])

for (t, d) in data.items():
    d = np.array(d)
    plt.plot(d[:, 0], d[:, 1], label=f't={t}')
plt.xlabel('g')
plt.ylabel('Tc/g²')
plt.ylim((0, None))
plt.tight_layout()
if len(data) > 1:
    plt.legend()
plt.savefig('tc.pdf', dpi=100)
plt.show()
