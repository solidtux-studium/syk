#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import json
from glob import glob
import re
import masterdb
import sys

fig, ax = plt.subplots(2, 2, sharex=True, figsize=(16,12))

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

try:
    ref_lam = []
    r = re.compile(r'.*beta(\d*\.\d*)\.out')
    r2 = re.compile(r'.*lam_sc.*=.* (\d*\.\d*)')
    for fn in glob('/home/daniel/Programme/syk_ilya/test/*.out'):
        m = r.match(fn)
        if m is not None:
            beta = float(m.group(1))
            with open(fn, 'r') as f:
                lam = None
                for l in f.readlines():
                    m2 = r2.match(l)
                    if m2 is not None:
                        lam = float(m2.group(1))
                        break
            ref_lam.append([beta,lam])
    ref_lam.sort(key=lambda x: x[0])
    ref_lam = np.atleast_2d(np.array(ref_lam))
    ax[0,0].plot(ref_lam[:,0], ref_lam[:,1], '-', lw=2, label='Ilya', color='k')
except Exception as e:
    print(e)

files = glob(f'{output}/*')
files.sort()
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    g = info['parameters']['g']
    label = f't={t}\ng={g}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_real_data(f.read())
        beta = data[:,0]
        lam = np.array(data[:,1])
        dist = data[:,2]
        # lam[dist > 1e-7] = np.nan
        it = data[:,3]
        alphac = 1. - 1./lam
        ax[0,0].plot(beta, lam, '+-', lw=0.5, label=label)
        ax[0,1].plot(beta, lam/np.nanmax(lam), '+-', lw=0.5, label=label)
        ax[1,0].semilogy(beta[~np.isnan(dist)], dist[~np.isnan(dist)], '+-', lw=0.5, label=label)
        ax[1,1].plot(beta, it, '+-', lw=0.5, label=label)

ax[0,0].set_ylim(-0.125, 1.525)
ax[1,0].set_xlabel('β')
ax[1,1].set_xlabel('β')
ax[0,0].set_ylabel('λ')
ax[0,1].set_ylabel('λ/λmax')
ax[1,0].set_ylabel('dist')
ax[1,1].set_ylabel('iter')

ax[1,1].legend(ncol=3)

plt.tight_layout()
plt.savefig('alphac.png', dpi=100)
