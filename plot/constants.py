#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.optimize as opt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

plotf = plt.plot

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

fitstart = 5
fitend = 200
# fitstart = 0.01
# fitend = 0.5

def linear(x, c, d):
    return np.log(c) + (1-2*d)*x

files = glob(f'{output}/fields_*')
files.sort()
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    g = info['parameters']['g']
    beta = info['parameters']['beta']
    omega0 = info['parameters']['config']['omega0']
    label = f't={t} g={g} ω0={omega0} β={beta}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_complex_data(f.read())
        df = pd.DataFrame(data, columns=info['quantities'])
        x = df['omegaf']
        y = np.imag(1./df['G_omega']) - x
        plotf(x, y, '+:', label=label)

        try:
            fitx = x[(x > fitstart) & (x < fitend)]
            fity = y[(x > fitstart) & (x < fitend)]
            popt, pcov = opt.curve_fit(linear, np.log(fitx), np.log(fity))
            perr = np.sqrt(np.diag(pcov))
            c = popt[0]/(g**(4*popt[1]))
            print(f'{label} c={c:.4f} Δ={popt[1]:.4f}±{perr[1]:.4f}')
            plotf(fitx, np.exp(linear(np.log(fitx), *popt)), label=f'fit {label}')
        except Exception as e:
            print(e)
plt.legend()
plt.tight_layout()
plt.show()
