#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.optimize as opt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

plotf = plt.loglog

plt.figure(figsize=(16,12))

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

files = glob(f'{output}/fields_*')
files.sort()
xmin = np.inf
xmax = 0
tvals = []
gvals = []
betavals = [10, 100, 500, 1000]
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    omega0 = info['parameters']['config']['omega0']
    g = info['parameters']['g']/(omega0**1.5)
    beta = info['parameters']['beta']
    skip = True
    for b in betavals:
        skip = skip and np.abs(beta - b) > 0.2
    if skip:
        continue
    label = f't={t:.2f} g={g:.2f} ω0={omega0:.2f} β={beta:.2f}'
    tvals.append(t)
    gvals.append(g)
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_complex_data(f.read())
        df = pd.DataFrame(data, columns=info['quantities'])
        x = df['omegaf']
        xmin = min(np.nanmin(np.abs(x)), xmin)
        xmax = max(np.nanmax(np.abs(x)), xmax)
        y = -df['G_omega'].to_numpy().imag
        plotf(x/g**2, y*g**2, '+', label=label, zorder=1)
delta = 0.4203
c1 = 1.1547
x = np.exp(np.linspace(np.log(xmin), np.log(xmax), 100))
# for t in set(tvals):
    # if t == 0:
        # continue
    # try:
        # y = 2./(x+np.sqrt(4*t**2+x**2))
        # plotf(x, y, '-', zorder=0, label=f't={t} g=0')
    # except Exception as e:
        # print(e)
for c1 in [0.832260, 1.1547005]:
    y = 1./(x*(1+c1*np.abs(1/x)**(2*delta)))
    plotf(x, y, '-', zorder=0, label=f'c1={c1}')
y = 1./(x*c1*np.abs(1/x)**(2*delta))
plotf(x, y, '-', zorder=0)
# y = 1./x
# plotf(x, y, '-', zorder=0, label=f't=0 g=0')
# plt.xlim(None, 1)
# plt.ylim(0.5, 2.5)
plt.legend()
plt.tight_layout()
plt.savefig('paper.png', dpi=100)
