#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

fig = plt.figure(figsize=(16, 12))

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

files = glob(f'{output}/*')
files.sort()
values = {}
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    g = info['parameters']['g']
    label = f't={t}\ng={g}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_real_data(f.read())
        beta = data[:, 0]
        lam = np.array(data[:, 1])
        dist = data[:, 2]
        # lam = lam[dist > 1e-7]
        # beta = beta[dist > 1e-7]
        # dist = dist[dist > 1e-7]
        if len(lam) > 1:
            for i in range(len(beta)):
                values[(beta[i], g)] = lam[i]

x = []
y = []
z = []
for ((beta, g), lam) in values.items():
    x.append(beta)
    y.append(g)
    z.append(lam)

# plt.tricontourf(x, y, z, 1000)
plt.tripcolor(x, y, z)
# plt.plot(x, y, 'r+', ms=10)
plt.colorbar()
plt.xlabel('β')
plt.ylabel('g')
plt.tight_layout()
plt.savefig('lambda.png', dpi=100)
