#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

delta = 0.420374
c2 = 0.561228
wrange = 3
style = '-'
lw = 1

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

files = glob(f'{output}/*')
files.sort()
wr0 = {}
for fn in files:
    if 'fields' in fn:
        continue
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    if info['session']['run']['host'] != 'tkmnc14':
        continue
    t = info['parameters']['t']
    omega0 = info['parameters']['config']['omega0']
    g = info['parameters']['g']/(omega0**1.5)
    g = round(g, 3)
    tc = 8*g**2/(3*np.pi)
    if g not in wr0.keys():
        wr0[g] = []
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_real_data(f.read())
        if data.shape[1] >= 5:
            beta = data[:,0]
            dist = data[:,2]
            pi0 = np.copy(data[:,4])
            pi0[dist > 1e-2] = np.nan
            tg = 1./(beta*g**2)
            wr = omega0**2 - pi0
            wrend = wr[tg < wrange*np.amin(tg)]
            tgend = tg[tg < wrange*np.amin(tg)]
            try:
                def f(x, a, b, c):
                    return a*np.power(x,2) + b*x + np.log(c)
                popt, pcov = opt.curve_fit(f, np.log(tgend), np.log(wrend))
                n = len(wrend)
                wr0[g].append([t/tc, *popt])
            except Exception as e:
                print(e)

fig, ax = plt.subplots(3, 1, sharex=True, figsize=(16,12))
# fig.suptitle('Fit to low temperature part of log(ωr)(log(T)).')
ax[0].axvline(1, ls=':', color='k')
ax[1].axvline(1, ls=':', color='k')
ax[2].axvline(1, ls=':', color='k')
for (g, d) in wr0.items():
    d.sort(key=lambda x: x[0])
    d = np.array(d)
    ax[0].plot(d[:,0], d[:,1], style, label=f'g={g}', lw=lw)
    ax[1].semilogy(d[:,0], np.abs(d[:,2]), style, label=f'g={g}', lw=lw)
    ax[2].semilogy(d[:,0], d[:,3], style, label=f'g={g}', lw=lw)
x = np.linspace(1, 16, 200)
y = 1 - 1/x
ax[2].autoscale(False)
ax[2].semilogy(x, y, 'k--', label='1-tc/t')
ax[0].set_title('quadratic')
ax[1].set_title('linear')
ax[2].set_title('constant')
ax[2].set_xlabel('t/tc')
# ax[2].set_ylim(None, 1)
ax[2].legend(bbox_to_anchor=(0.5,-0.3),loc='center',ncol=5)
plt.tight_layout(rect=(0,0.05,1,1))
plt.savefig('wr0.png', dpi=100)
fig.set_size_inches(8, 10)
plt.tight_layout(rect=(0,0,1,1))
plt.savefig('wr0.pdf')
