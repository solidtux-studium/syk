#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.optimize as opt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
fig = plt.figure(figsize=(16, 12))
ax = np.array([[None] * 2] * 2)
ax[0, 0] = fig.add_subplot(2, 2, 1)
ax[0, 1] = fig.add_subplot(2, 2, 2)
ax[1, 0] = fig.add_subplot(2, 2, 3, sharex=ax[0, 0])
ax[1, 1] = fig.add_subplot(2, 2, 4, sharex=ax[0, 1])

files = glob(f'{output}/fields_*')
files.sort()
i = 0
parameters = set()
for fn in files:
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    t = info['parameters']['t']
    g = info['parameters']['g']
    beta = info['parameters']['beta']
    omega0 = info['parameters']['config']['omega0']
    label = f't={t} g={g} ω0={omega0} β={beta}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_complex_data(f.read())
        df = pd.DataFrame(data, columns=info['quantities'])
        for (q, x) in [
                # ('Sigma', 'f'),
                ('G', 'f'),
        ]:
            ax[0, 0].plot(df[f'omega{x}'], np.real(
                df[f'{q}_omega']), label=f'{q} {label}',
                color=colors[i])
            ax[1, 0].plot(df[f'omega{x}'], np.imag(
                df[f'{q}_omega']), label=f'{q} {label}',
                color=colors[i])
            ax[0, 1].plot(df['tau'], np.real(
                df[f'{q}_tau']), label=f'{q} {label}',
                color=colors[i])
            ax[1, 1].plot(df['tau'], np.imag(
                df[f'{q}_tau']), label=f'{q} {label}',
                color=colors[i])
            # ax[0, 1].plot(df['tau']-beta, -np.real(
            # df[f'{q}_tau']), color=colors[i])
            # ax[1, 1].plot(df['tau']-beta, -np.imag(
            # df[f'{q}_tau']), color=colors[i])
            parameters.add((beta, g, i))
            i += 1

ax[0, 1].autoscale(False)
ax[1, 1].autoscale(False)
for (beta, g, i) in parameters:
    def analytic(x, phi):
        x = x.astype(np.complex)
        c = np.pi**0.25/np.sqrt(2*g)
        res = c * np.power(np.tan(np.pi/4 + phi), -x/beta)
        res[x.real > 0] *= -np.sin(np.pi/4 + phi) / \
            np.sqrt(beta * np.sin(np.pi*x/beta))
        res[x <= 0] *= np.cos(np.pi/4 + phi) / \
            np.sqrt(beta * np.sin(-np.pi*x/beta))
        return res

    ax[0, 1].plot(df['tau'], np.real(analytic(df['tau'], 0)),
                  color=colors[i], label=f'analytic β={beta} g={g}', ls=':')
    # ax[0, 1].plot(-df['tau'], np.real(analytic(-df['tau'], 0)),
    # color=colors[i], ls=':')
    ax[1, 1].plot(df['tau'], np.imag(analytic(df['tau'], 0)),
                  color=colors[i], label=f'analytic β={beta} g={g}', ls=':')
    # ax[1, 1].plot(-df['tau'], np.imag(analytic(-df['tau'], 0)),
    # color=colors[i], ls=':')

ax[0, 0].set_title('real')
ax[0, 1].set_title('real')
ax[1, 0].set_title('imag')
ax[1, 1].set_title('imag')
ax[1, 0].set_xlabel('ω')
ax[1, 1].set_xlabel('τ')
ax[0, 0].legend()
ax[0, 1].legend()
plt.tight_layout()
plt.show()
