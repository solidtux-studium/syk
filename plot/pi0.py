#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from matplotlib.colors import LogNorm
import json
from glob import glob
import re
import masterdb
import sys

plotf = plt.semilogx
fig = plt.figure(figsize=(16,12))
delta = 0.420374
c2 = 0.561228
ref = False

output = 'output'
if len(sys.argv) > 1:
    output = sys.argv[1]

if ref:
    files = glob('/home/daniel/Programme/syk_ilya/test/*piq0.out')
    files.sort()
    try:
        ref = []
        g = 1.0854018818374014890
        omega0 = 1
        r = re.compile(r'.*beta(\d*\.\d*)_piq0\.out')
        r2 = re.compile(r'.*([\d-]\d*\.\d*)')
        for fn in files:
            m = r.match(fn)
            if m is not None:
                beta = float(m.group(1))
                with open(fn, 'r') as f:
                    pi0 = None
                    for l in f.readlines():
                        m2 = r2.match(l)
                        if m2 is not None:
                            pi0 = float(m2.group(1))
                            break
                if pi0 is not None:
                    ref.append([1/(beta*g**2), omega0**2 - pi0])
        ref.sort(key=lambda x: x[0])
        ref = np.array(ref)
        plotf(ref[:,0], ref[:,1], '-', lw=2, label='Ilya', color='k', zorder=0)
    except Exception as e:
        print(e)

files = glob(f'{output}/*')
files.sort()
curves = {}
ref = {}
for fn in files:
    if 'fields' in fn:
        continue
    with open(f'{fn}/info.json', 'r') as f:
        info = json.load(f)
    omega0 = info['parameters']['config']['omega0']
    g = info['parameters']['g']/(omega0**1.5)
    t = info['parameters']['t']
    tc = 8*g**2/(3*np.pi)
    t = round(t/tc, 2)
    label = f't={t}\ng={g}'
    with open(f'{fn}/data', 'rb') as f:
        data = masterdb.decode_real_data(f.read())
        if data.shape[1] >= 5:
            beta = data[:,0]
            dist = data[:,2]
            pi0 = np.copy(data[:,4])
            pi0[dist > 1e-2] = np.nan
            tg = 1./(beta*g**2)
            wr = omega0**2 - pi0
            if t == 0:
                ref[g] = wr
            else:
                curves[(t, g)] = (tg, wr)

for ((t, g), (tg, wr)) in curves.items():
    plotf(tg, wr-ref[g], '-', label=f't={t} g={g} ω0={omega0}', zorder=1)

plt.autoscale(False)
x = np.logspace(-6, 4, 500)
# x = np.logspace(-1, 1, 100)
y = c2*np.power(x, 4*delta - 1)
plotf(x, y, 'k:', zorder=0)
y = (3*np.pi/8)**2 * x
plotf(x, y, 'k:', zorder=0)
plt.xlabel('T/g²')
plt.ylabel('ωr²=ω₀² - Π(0)')
# plt.xlim(0.001, 10)
# plt.ylim(0.001, 1)
plt.legend()
plt.tight_layout()
plt.savefig('pi0.png', dpi=100)
