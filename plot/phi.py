#!/usr/bin/env python3

import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, Normalize
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots(2, 2)

phi = np.atleast_2d(np.genfromtxt('phi.txt'))
phic = phi - 1
dist = np.atleast_2d(np.genfromtxt('dist.txt'))
it = np.atleast_2d(np.genfromtxt('iter.txt'))
b = np.atleast_2d(np.genfromtxt('beta.txt'))
g = np.atleast_2d(np.genfromtxt('g.txt'))

phi = np.ma.masked_where(np.isnan(phi), phi)
phic = np.ma.masked_where(np.isnan(phic), phic)
dist = np.ma.masked_where(np.isnan(dist), dist)
phi = np.ma.masked_where(np.isinf(phi), phi)
phic = np.ma.masked_where(np.isinf(phic), phic)
dist = np.ma.masked_where(np.isinf(dist), dist)

pcm1 = ax[0,0].pcolormesh(g, b, phi, norm=LogNorm())
fig.colorbar(pcm1, ax=ax[0,0], orientation='horizontal')

pcm2 = ax[0,1].pcolormesh(g, b, phic, norm=Normalize(vmin=-1, vmax=1), cmap='seismic')
fig.colorbar(pcm2, ax=ax[0,1], orientation='horizontal')

pcm3 = ax[1,0].pcolormesh(g, b, dist, norm=LogNorm())
fig.colorbar(pcm3, ax=ax[1,0], orientation='horizontal')

pcm4 = ax[1,1].pcolormesh(g, b, it)
fig.colorbar(pcm4, ax=ax[1,1], orientation='horizontal')

for i in range(2):
    for j in range(2):
        ax[i,j].set_xlabel('g')
        ax[i,j].set_ylabel('β')

ax[0,0].set_title('Φ')
ax[0,1].set_title('Φc')
ax[1,0].set_title('distance')
ax[1,1].set_title('iterations')

plt.show()
