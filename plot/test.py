#!/usr/bin/env python3

import numpy as np
import os.path
from glob import glob
import matplotlib.pyplot as plt

func = np.abs

files = glob('target/release/build/*/out/fft_gauss.txt')
f = files[0]
print(files, f)
d = os.path.getmtime(f)
for fn in files:
    if os.path.getmtime(fn) > d:
        d = os.path.getmtime(fn)
        f = fn

data = np.genfromtxt(f)
tau = data[:,0]
x = data[:,1] + 1j*data[:,2]
x2 = data[:,3] + 1j*data[:,4]
diffx = data[:,5]
omega = data[:,6]
y = data[:,7] + 1j*data[:,8]
y2 = data[:,9] + 1j*data[:,10]
diffy = data[:,11]

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.set_title('tau')
ax1.plot(tau, func(x2), '+-', lw=0.5, label='2')
ax1.plot(tau, func(x), '+-', lw=0.5, label='1')
ax1.set_ylim(np.amin(func(x)), np.amax(func(x)))
ax1.legend()
ax2.plot(tau, diffx)
plt.tight_layout()

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
ax1.set_title('omega')
ax1.plot(omega, func(y2), '+-', lw=0.5, label='2')
ax1.plot(omega, func(y), '+-', lw=0.5, label='1')
ax1.set_ylim(np.amin(func(y)), np.amax(func(y)))
ax1.legend()
ax2.plot(omega, diffy)
plt.tight_layout()

plt.figure()
plt.plot(omega, np.real(y), '+-', lw=0.5, label='re')
plt.plot(omega, np.imag(y), '+-', lw=0.5, label='im')
plt.legend()
plt.tight_layout()

plt.show()
