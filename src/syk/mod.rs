mod fermion;
pub mod fft;
mod phonon;

pub use fermion::Fermion;
pub use phonon::Phonon;

use self::fft::FFTType;
use log::{error, info, trace};
use ndarray::prelude::*;
use ndarray_rand::{rand_distr::*, RandomExt};
use num_complex::Complex64;
use serde::{Deserialize, Serialize};
use std::{
    f64::{consts::PI, INFINITY, NAN},
    fs::File,
    io::{self, Write},
    path::Path,
};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum ModelType {
    Phonon,
    Fermion,
}

impl ModelType {
    pub fn model(&self) -> Box<dyn Model> {
        match self {
            ModelType::Phonon => Box::new(Phonon),
            ModelType::Fermion => Box::new(Fermion),
        }
    }
}

impl Default for ModelType {
    fn default() -> Self {
        ModelType::Phonon
    }
}

pub trait Model {
    fn run(
        &self,
        f: &mut Fields,
        beta: f64,
        g: f64,
        t: f64,
        omega0: f64,
        eps: f64,
        n: usize,
        mix: f64,
        min_mix: f64,
        maxiter: usize,
        calculate_lambda: bool,
        fft: &FFTType,
    ) -> Output;
}

pub struct Field {
    bosonic: bool,
    pub tau: Array1<Complex64>,
    pub omega: Array1<Complex64>,
}

pub struct Fields {
    pub g: Field,
    pub f: Field,
    pub d: Field,
    pub sigma: Field,
    pub phi: Field,
    pub pi: Field,
}

impl Field {
    pub fn zero(n: usize, bosonic: bool) -> Self {
        Field {
            bosonic,
            tau: Array1::zeros(n),
            omega: Array1::zeros(n),
        }
    }

    pub fn random(n: usize, bosonic: bool) -> Self {
        Field {
            bosonic,
            tau: rand_cmpl(n),
            omega: rand_cmpl(n),
        }
    }

    pub fn write<P: AsRef<Path>>(&self, beta: f64, p: P) -> Result<(), io::Error> {
        let mut f = File::create(p)?;
        let n = self.tau.len();

        for i in 0..n {
            writeln!(
                f,
                "{} {} {} {} {} {}",
                tau(i, n, beta),
                self.tau[i].re,
                self.tau[i].im,
                omega(i, n, beta, self.bosonic),
                self.omega[i].re,
                self.omega[i].im
            )?;
        }

        Ok(())
    }
}

impl Fields {
    pub fn zero(n: usize) -> Self {
        Fields {
            g: Field::zero(n, false),
            f: Field::zero(n, false),
            d: Field::zero(n, true),
            sigma: Field::zero(n, false),
            phi: Field::zero(n, false),
            pi: Field::zero(n, true),
        }
    }

    pub fn random(n: usize) -> Self {
        Fields {
            g: Field::random(n, false),
            f: Field::random(n, false),
            d: Field::random(n, true),
            sigma: Field::random(n, false),
            phi: Field::random(n, false),
            pi: Field::random(n, true),
        }
    }

    pub fn array(&self, beta: f64) -> Array2<Complex64> {
        let n = self.g.omega.len();
        let mut res = Array2::zeros((n, 15));
        for i in 0..n {
            res[[i, 0]] = omega(i, n, beta, false).into();
            res[[i, 1]] = omega(i, n, beta, true).into();
            res[[i, 2]] = tau(i, n, beta).into();
        }
        res.slice_mut(s![.., 3]).assign(&self.g.omega);
        res.slice_mut(s![.., 4]).assign(&self.g.tau);
        res.slice_mut(s![.., 5]).assign(&self.f.omega);
        res.slice_mut(s![.., 6]).assign(&self.f.tau);
        res.slice_mut(s![.., 7]).assign(&self.d.omega);
        res.slice_mut(s![.., 8]).assign(&self.d.tau);
        res.slice_mut(s![.., 9]).assign(&self.sigma.omega);
        res.slice_mut(s![.., 10]).assign(&self.sigma.tau);
        res.slice_mut(s![.., 11]).assign(&self.phi.omega);
        res.slice_mut(s![.., 12]).assign(&self.phi.tau);
        res.slice_mut(s![.., 13]).assign(&self.pi.omega);
        res.slice_mut(s![.., 14]).assign(&self.pi.tau);
        res
    }
}

pub fn omega(i: usize, n: usize, beta: f64, bosonic: bool) -> f64 {
    if bosonic {
        (2. * (i as f64) - n as f64 + 2.) * PI / beta
    } else {
        (2. * (i as f64) - n as f64 + 1.) * PI / beta
    }
}

pub fn tau(i: usize, n: usize, beta: f64) -> f64 {
    beta * (i as f64) / (n as f64)
}

fn rand_cmpl(n: usize) -> Array1<Complex64> {
    Array1::random(n, UnitDisc {}).mapv(|x| Complex64::new(x[0], x[1]))
}

pub struct Output {
    pub lambda: f64,
    pub dist: f64,
    pub iter: usize,
    pub mix: f64,
}

#[cfg(test)]
mod test {
    use super::*;

    use std::{fs::File, io::Write, time::Instant};

    const N: usize = 1 << 13;
    const EPS: f64 = 1e-10;

    #[test]
    fn ferm_omega_sum() {
        assert!((0..N).map(|i| omega(i, N, 1., false)).sum::<f64>() < EPS);
    }

    #[test]
    fn fft_gauss() {
        let beta = 0.1;
        let x = Array1::from(
            (0..N)
                .map(|i| {
                    Complex64::new(
                        (-tau(i, N, beta).powi(2)).exp()
                            - (-(tau(i, N, beta) - beta).powi(2)).exp(),
                        0.0,
                    )
                })
                .collect::<Vec<_>>(),
        );
        let start = Instant::now();
        let y = fft(&x.view(), false, false, beta);
        let x2 = fft(&y.view(), false, true, beta);
        let y2 = fft(&x2.view(), false, false, beta);
        println!("{:?}", Instant::now() - start);
        let mut f = File::create(format!("{}/fft_gauss.txt", env!("OUT_DIR"))).unwrap();
        for i in 0..N {
            writeln!(
                f,
                "{} {} {} {} {} {} {} {} {} {} {} {}",
                tau(i, N, beta),
                x[i].re,
                x[i].im,
                x2[i].re,
                x2[i].im,
                (x[i] - x2[i]).norm(),
                omega(i, N, beta, false),
                y[i].re,
                y[i].im,
                y2[i].re,
                y2[i].im,
                (y[i] - y2[i]).norm(),
            )
            .unwrap();
        }
        assert!((0..N).all(|i| (x[i] - x2[i]).norm() < EPS));
        assert!((0..N).all(|i| (y[i] - y2[i]).norm() < EPS));
    }
}
