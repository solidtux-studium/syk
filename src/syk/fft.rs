use chfft::CFft1D;
use fftw::{array::AlignedVec, plan::*, types::*};
use ndarray::prelude::*;
use num_complex::Complex64;
use rustfft::{FFTplanner, FFT};
use serde::{Deserialize, Serialize};
use std::{f64::consts::PI, str::FromStr, sync::Arc};

pub trait Plan {
    fn fft(
        &mut self,
        x: ArrayView1<Complex64>,
        y: ArrayViewMut1<Complex64>,
        bosonic: bool,
        inverse: bool,
        beta: f64,
    );
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum FFTType {
    Chfft,
    Rustfft,
    Fftw,
}

impl Default for FFTType {
    fn default() -> Self {
        FFTType::Chfft
    }
}

impl FromStr for FFTType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "chfft" => Ok(FFTType::Chfft),
            "rustfft" => Ok(FFTType::Rustfft),
            "fftw" => Ok(FFTType::Fftw),
            x => Err(format!("Unknown FFT type {}.", x)),
        }
    }
}

impl FFTType {
    pub fn new_plan(&self, n: usize) -> Box<dyn Plan> {
        match self {
            FFTType::Chfft => Box::new(ChPlan::new(n)),
            FFTType::Fftw => Box::new(FftwPlan::new(n)),
            FFTType::Rustfft => Box::new(RustPlan::new(n)),
        }
    }
}

pub struct ChPlan {
    fft: CFft1D<f64>,
}

impl ChPlan {
    fn new(n: usize) -> Self {
        ChPlan {
            fft: CFft1D::<f64>::with_len(n),
        }
    }
}

impl Plan for ChPlan {
    fn fft(
        &mut self,
        x: ArrayView1<Complex64>,
        mut y: ArrayViewMut1<Complex64>,
        bosonic: bool,
        inverse: bool,
        beta: f64,
    ) {
        y.assign(&x);
        let n = x.len();
        let a = if bosonic { 2. } else { 1. };
        if inverse {
            self.fft.forward0i(y.as_slice_mut().unwrap());
            for i in 0..n {
                y[i] *= Complex64::new(0., PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            y.mapv_inplace(|x| x / beta);
        } else {
            for i in 0..n {
                y[i] *= Complex64::new(0., -PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            self.fft.backward0i(y.as_slice_mut().unwrap());
            y.mapv_inplace(|x| x * beta / (n as f64));
        }
    }
}

pub struct RustPlan {
    fft: Arc<dyn FFT<f64>>,
    ifft: Arc<dyn FFT<f64>>,
}

impl RustPlan {
    fn new(n: usize) -> Self {
        let mut planner = FFTplanner::new(false);
        let mut iplanner = FFTplanner::new(true);
        RustPlan {
            fft: planner.plan_fft(n),
            ifft: iplanner.plan_fft(n),
        }
    }
}

impl Plan for RustPlan {
    fn fft(
        &mut self,
        x: ArrayView1<Complex64>,
        mut y: ArrayViewMut1<Complex64>,
        bosonic: bool,
        inverse: bool,
        beta: f64,
    ) {
        let n = x.len();

        let a = if bosonic { 2. } else { 1. };
        let mut x = x.to_owned();
        if inverse {
            self.ifft
                .process(x.as_slice_mut().unwrap(), y.as_slice_mut().unwrap());
            for i in 0..n {
                y[i] *= Complex64::new(0., PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            y.mapv_inplace(|x| x / beta);
        } else {
            for i in 0..n {
                x[i] *= Complex64::new(0., -PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            self.fft
                .process(x.as_slice_mut().unwrap(), y.as_slice_mut().unwrap());
            y.mapv_inplace(|x| x * beta / (n as f64));
        }
    }
}

pub struct FftwPlan {
    plan: C2CPlan64,
    iplan: C2CPlan64,
    inp: AlignedVec<Complex64>,
    out: AlignedVec<Complex64>,
}

impl FftwPlan {
    fn new(n: usize) -> Self {
        FftwPlan {
            plan: C2CPlan::aligned(&[n], Sign::Backward, Flag::Measure).unwrap(),
            iplan: C2CPlan::aligned(&[n], Sign::Forward, Flag::Measure).unwrap(),
            inp: AlignedVec::new(n),
            out: AlignedVec::new(n),
        }
    }
}

impl Plan for FftwPlan {
    fn fft(
        &mut self,
        x: ArrayView1<Complex64>,
        mut y: ArrayViewMut1<Complex64>,
        bosonic: bool,
        inverse: bool,
        beta: f64,
    ) {
        let n = x.len();

        let a = if bosonic { 2. } else { 1. };

        for i in 0..n {
            self.inp[i] = x[i];
        }
        if inverse {
            self.iplan.c2c(&mut self.inp, &mut self.out).unwrap();
            for i in 0..n {
                y[i] = self.out[i];
                y[i] *= Complex64::new(0., PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            y.mapv_inplace(|x| x / beta);
        } else {
            for i in 0..n {
                self.inp[i] *= Complex64::new(0., -PI * (i as f64) * (1. - a / (n as f64))).exp();
            }
            self.plan.c2c(&mut self.inp, &mut self.out).unwrap();
            for i in 0..n {
                y[i] = self.out[i]
            }
            y.mapv_inplace(|x| x * beta / (n as f64));
        }
    }
}
