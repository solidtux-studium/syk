use super::{fft::*, *};

pub struct Phonon;

impl Model for Phonon {
    fn run(
        &self,
        f: &mut Fields,
        beta: f64,
        g: f64,
        t: f64,
        omega0: f64,
        eps: f64,
        n: usize,
        mix: f64,
        min_mix: f64,
        maxiter: usize,
        calculate_lambda: bool,
        fft: &FFTType,
    ) -> Output {
        debug_assert_eq!(n % 2, 0);
        info!(
            "β={} g={} ω0={} ε={:e} n={} maxiter={}",
            beta, g, omega0, eps, n, maxiter
        );
        let mut mix = mix;
        let mut dist = INFINITY;
        let mut old = Array2::zeros((2, n));
        let mut iter = maxiter;
        let mut om_f = Array1::zeros(n);
        let mut om_b = Array1::zeros(n);
        let mut pi_tmp = Array1::zeros(n);
        let im = Complex64::i();
        let mut err_count = 0;
        let mut plan = fft.new_plan(n);

        for i in 0..n {
            om_f[i] = Complex64::new(omega(i, n, beta, false), 0.);
            om_b[i] = Complex64::new(omega(i, n, beta, true), 0.);
        }

        f.g.omega = (im * &om_f - &f.sigma.omega).map(|&x| 1. / x);
        f.d.omega = (&om_b.map(|&x| x.powi(2)) + omega0.powi(2) - &f.pi.omega).map(|&x| 1. / x);

        // Solve normal state equation
        let mut last_dist = INFINITY;
        for i in 0..maxiter {
            old.slice_mut(s![0, ..]).assign(&f.sigma.omega);
            old.slice_mut(s![1, ..]).assign(&f.pi.omega);

            plan.fft(f.g.omega.view(), f.g.tau.view_mut(), false, true, beta);
            plan.fft(f.d.omega.view(), f.d.tau.view_mut(), true, true, beta);

            for i in 0..n {
                f.sigma.tau[i] = f.g.tau[i] * f.d.tau[i] * g.powi(2) + f.g.tau[i] * t.powi(2);
                f.pi.tau[i] = f.g.tau[i] * f.g.tau[(n - i) % n] * 2. * g.powi(2);
            }

            plan.fft(
                f.sigma.tau.view(),
                f.sigma.omega.view_mut(),
                false,
                false,
                beta,
            );
            plan.fft(f.pi.tau.view(), f.pi.omega.view_mut(), true, false, beta);

            let dist_sig = (&f.sigma.omega - &old.slice(s![0, ..])).fold(NAN, |acc, x| {
                let n = x.norm_sqr();
                if n > acc || acc.is_nan() {
                    n
                } else {
                    acc
                }
            });
            let dist_pi = (&f.pi.omega - &old.slice(s![1, ..])).fold(NAN, |acc, x| {
                let n = x.norm_sqr();
                if n > acc || acc.is_nan() {
                    n
                } else {
                    acc
                }
            });
            dist = if dist_sig > dist_pi {
                dist_sig
            } else {
                dist_pi
            };

            let mut finished = false;
            while !finished {
                pi_tmp = &f.pi.omega * mix + &old.slice(s![1, ..]) * (1. - mix);

                f.d.omega = (&om_b.map(|&x| x.powi(2)) + omega0.powi(2) - &pi_tmp).map(|&x| 1. / x);

                if f.d.omega.iter().any(|x| x.re < 0.) && mix > min_mix {
                    mix *= 0.8;
                    info!("D(omega) has wrong sign. mix -> {}", mix);
                    finished = false;
                } else {
                    finished = true;
                }
            }
            f.pi.omega.assign(&pi_tmp);
            f.sigma.omega = &f.sigma.omega * mix + &old.slice(s![0, ..]) * (1. - mix);
            f.g.omega = (im * &om_f - &f.sigma.omega).map(|&x| 1. / x);

            trace!("Distance {}.", dist);
            if dist < eps || dist.is_nan() {
                iter = i;
                break;
            }
            if dist > last_dist && mix > min_mix {
                err_count += 1;
                if err_count > 100 {
                    err_count = 0;
                    mix *= 0.8;
                    info!("Error increased. mix -> {}", mix);
                }
            }
            last_dist = dist;
        }
        info!("Distance {:e} after {} iterations.", dist, iter);
        if dist.is_nan() || !calculate_lambda {
            return Output {
                lambda: NAN,
                dist,
                iter,
                mix,
            };
        }

        let mut lambda = NAN;
        match arpack_ng::eigenvectors(
            |phi1, mut phi2| {
                for i in 0..n {
                    f.f.omega[i] = -phi1[i] / (om_f[i] + im * f.sigma.omega[i]).powi(2);
                }
                plan.fft(f.f.omega.view(), f.f.tau.view_mut(), false, true, beta);
                for i in 0..n {
                    f.phi.tau[i] = -f.d.tau[i] * g.powi(2) * f.f.tau[i];
                }
                plan.fft(f.phi.tau.view(), f.phi.omega.view_mut(), false, false, beta);
                phi2.assign(&f.phi.omega);
            },
            n,
            1,
            4.min(n),
            300,
        ) {
            Ok((val, _)) => lambda = val[0].norm(),
            Err(e) => error!("{:?}", e),
        }

        Output {
            lambda,
            dist,
            iter,
            mix,
        }
    }
}
