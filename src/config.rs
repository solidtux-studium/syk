use crate::{fft::FFTType, syk::ModelType};
use ndarray::prelude::*;
use num_traits::Float;
use serde::{Deserialize, Serialize};
use std::{
    error::Error,
    fs::File,
    path::{Path, PathBuf},
};

const fn default_eps() -> f64 {
    std::f64::EPSILON
}

const fn default_min_mix() -> f64 {
    1e-3
}

const fn default_lambda_max() -> f64 {
    1.
}

fn default_output() -> PathBuf {
    "output".into()
}

const fn default_min_n() -> usize {
    16
}

const fn default_rel_tc() -> bool {
    false
}

const fn default_rel_tg() -> bool {
    false
}

const fn default_calculate_lambda() -> bool {
    true
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum TempRange {
    BetaLin(f64, f64, f64),
    BetaGeom(f64, f64, usize),
    TLin(f64, f64, f64),
    TGeom(f64, f64, usize),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Range<T> {
    Lin(T, T, T),
    Geom(T, T, usize),
    List(Vec<T>),
}

impl<T: Float> Range<T> {
    pub fn to_array(&self) -> Array1<T> {
        match self {
            &Range::Lin(min, max, step) => Array1::range(min, max + step, step),
            &Range::Geom(min, max, steps) => Array1::geomspace(min, max, steps).unwrap(),
            Range::List(v) => v.clone().into(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Config {
    #[serde(default)]
    pub model: ModelType,
    #[serde(default)]
    pub fft: FFTType,
    pub g: Range<f64>,
    pub t: Range<f64>,
    #[serde(default = "default_rel_tc")]
    pub rel_tc: bool,
    #[serde(default = "default_rel_tg")]
    pub rel_tg: bool,
    pub omega0: f64,
    pub dt: f64,
    #[serde(default = "default_min_mix")]
    pub min_mix: f64,
    pub maxiter: usize,
    pub temp: Vec<TempRange>,
    #[serde(default = "default_eps")]
    pub eps: f64,
    #[serde(default = "default_output")]
    pub output: PathBuf,
    #[serde(default = "default_lambda_max")]
    pub lambda_max: f64,
    #[serde(default = "default_calculate_lambda")]
    pub calculate_lambda: bool,
    #[serde(default)]
    pub output_fields: FieldOutput,
    #[serde(default = "default_min_n")]
    pub min_n: usize,
    #[serde(default)]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(untagged)]
pub enum FieldOutput {
    Bool(bool),
    Filter(Vec<FieldOutputFilter>),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum FieldOutputFilter {
    BetaRange(f64, f64),
    Beta(f64),
}

impl Default for FieldOutput {
    fn default() -> Self {
        FieldOutput::Bool(false)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Parameters {
    pub config: Config,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub beta: Option<f64>,
    pub t: f64,
    pub g: f64,
}

impl Config {
    pub fn load(filename: &Path) -> Result<Config, Box<dyn Error>> {
        let f = File::open(filename)?;
        Ok(serde_yaml::from_reader(f)?)
    }
}
