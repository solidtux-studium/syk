mod config;
mod syk;

use crate::{config::*, syk::*};
use chrono::prelude::*;
use indicatif::{ProgressBar, ProgressStyle};
use log::warn;
use masterdb::{vergen_session, DataSet};
use ndarray::prelude::*;
use rayon::prelude::*;
use std::{error::Error, f64::consts::PI, fs, path::PathBuf, time::Instant};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(author, about)]
struct Opt {
    /// Path to configuration file.
    #[structopt(default_value = "syk.yaml")]
    config: PathBuf,
}

const BETA_DIFF_EPS: f64 = 1e-5;

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    let opt = Opt::from_args();
    let conf = Config::load(&opt.config)?;
    let session = vergen_session!();
    if let Err(e) = fs::create_dir_all(&conf.output) {
        warn!("{}", e);
    }

    let mut beta_val = Vec::new();
    for temp in &conf.temp {
        let tmp = match temp.clone() {
            TempRange::BetaLin(min, max, step) => Array1::range(min, max + step, step),
            TempRange::BetaGeom(min, max, steps) => Array1::geomspace(min, max, steps).unwrap(),
            TempRange::TLin(min, max, step) => Array1::range(max, min - step, -step)
                .map(|x| 1. / x)
                .to_owned(),
            TempRange::TGeom(min, max, steps) => Array1::geomspace(max, min, steps)
                .unwrap()
                .map(|x| 1. / x)
                .to_owned(),
        };
        if let Some(s) = tmp.as_slice() {
            beta_val.extend_from_slice(s);
        }
    }
    let t_vals = conf.t.to_array();
    let g_vals = conf.g.to_array();

    let bar = ProgressBar::new((beta_val.len() * t_vals.len() * g_vals.len()) as u64).with_style(
        ProgressStyle::default_bar().template(
            "{elapsed_precise} {wide_bar:.yellow/orange} {pos:>5}/{len:<5} ETA: {eta_precise}",
        ),
    );
    bar.tick();
    let start = Instant::now();
    g_vals.par_iter().for_each(|&g| {
        t_vals.par_iter().for_each(|&t| {
            let t = if conf.rel_tc {
                let tc = 8. * g.powi(2) / (3. * PI);
                t * tc
            } else {
                t
            };
            let mut fields = Fields::zero(0);
            let mut nold = 0;
            let mut first = true;
            let mut mix = 1.;
            let mut data = Array2::zeros((beta_val.len(), 5));
            let mut imax = 0;
            for (i, &beta) in beta_val.iter().enumerate() {
                let beta = if conf.rel_tg {
                    let gs = g / conf.omega0.powf(1.5);
                    beta / gs.powi(2)
                } else {
                    beta
                };
                imax = i;
                let mut n = (beta / conf.dt) as usize;
                n -= n % 2;
                n = n.max(conf.min_n);
                if first {
                    fields = Fields::zero(n);
                    first = false;
                } else {
                    let mut fnew = Fields::zero(n);
                    for i in 0..nold / 2 {
                        fnew.sigma.omega[n / 2 - i - 1] = fields.sigma.omega[nold / 2 - i - 1];
                        fnew.sigma.omega[n / 2 + i] = fields.sigma.omega[nold / 2 + i];
                        fnew.pi.omega[n / 2 - i - 1] = fields.pi.omega[nold / 2 - i - 1];
                        fnew.pi.omega[n / 2 + i] = fields.pi.omega[nold / 2 + i];
                    }
                    fields = fnew;
                }
                let res = conf.model.model().run(
                    &mut fields,
                    beta,
                    g,
                    t,
                    conf.omega0,
                    conf.eps,
                    n,
                    mix,
                    conf.min_mix,
                    conf.maxiter,
                    conf.calculate_lambda,
                    &conf.fft,
                );
                mix = res.mix;
                data[[i, 0]] = beta;
                data[[i, 1]] = res.lambda;
                data[[i, 2]] = res.dist;
                data[[i, 3]] = res.iter as f64;
                data[[i, 4]] = fields.pi.omega[n / 2 - 1].re;
                bar.inc(1);
                nold = n;
                let output_fields = match &conf.output_fields {
                    &FieldOutput::Bool(b) => b,
                    FieldOutput::Filter(v) => {
                        let mut res = false;
                        for filter in v {
                            match filter {
                                &FieldOutputFilter::BetaRange(a, b) => {
                                    res |= (beta >= a) && (beta <= b)
                                }
                                &FieldOutputFilter::Beta(a) => {
                                    res |= (beta - a).abs() <= BETA_DIFF_EPS
                                }
                            }
                        }
                        res
                    }
                };
                if output_fields {
                    DataSet::new(
                        session.clone(),
                        Parameters {
                            config: conf.clone(),
                            beta: Some(beta),
                            t,
                            g,
                        },
                        vec![
                            "omegaf".to_string(),
                            "omegab".to_string(),
                            "tau".to_string(),
                            "G_omega".to_string(),
                            "G_tau".to_string(),
                            "F_omega".to_string(),
                            "F_tau".to_string(),
                            "D_omega".to_string(),
                            "D_tau".to_string(),
                            "Sigma_omega".to_string(),
                            "Sigma_tau".to_string(),
                            "Phi_omega".to_string(),
                            "Phi_tau".to_string(),
                            "Pi_omega".to_string(),
                            "Pi_tau".to_string(),
                        ],
                        fields.array(beta).into_dyn(),
                    )
                    .unwrap()
                    .save(format!(
                        "{}/fields_{:020}_{:020}_{:020}_{}",
                        conf.output.display(),
                        (beta * 1e10) as u128,
                        (g * 1e10) as u128,
                        (t * 1e10) as u128,
                        Utc::now().timestamp(),
                    ))
                    .unwrap();
                }
                if res.lambda > conf.lambda_max {
                    bar.inc((beta_val.len() - imax - 1) as u64);
                    break;
                }
            }
            DataSet::new(
                session.clone(),
                Parameters {
                    config: conf.clone(),
                    beta: None,
                    t,
                    g,
                },
                vec![
                    "beta".to_string(),
                    "lambda".to_string(),
                    "dist".to_string(),
                    "iter".to_string(),
                    "pi0".to_string(),
                ],
                data.slice(s![..(imax + 1), ..]).to_owned().into_dyn(),
            )
            .unwrap()
            .save(format!(
                "{}/{:020}_{:020}_{}",
                conf.output.display(),
                (g * 1e10) as u128,
                (t * 1e10) as u128,
                Utc::now().timestamp(),
            ))
            .unwrap();
        })
    });
    bar.finish();
    println!("{:?}", Instant::now() - start);

    Ok(())
}
