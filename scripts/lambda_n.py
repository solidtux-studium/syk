#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import subprocess

def lam(t, g, n, maxiter, omega0):
    res = subprocess.run(['target/release/worker', f'{t}', f'{g}', f'{n}', f'{maxiter}', f'{omega0}'], stdout=subprocess.PIPE).stdout.decode('utf-8')
    parts = res.strip().split(' ')
    return (float(parts[0]), float(parts[1]), int(parts[2]))

g = 0.1
for omega0 in np.linspace(0.1, 1., 3.):
    px = []
    py = []
    tf = 0.189*g**2*omega0
    for x in np.logspace(-2., 0., 100.):
        y = lam(x*tf, g, 4096, 200, omega0)
        print(x, y)
        px.append(x)
        py.append(y[0])
    px = np.array(px)
    py = np.array(py)
    plt.loglog(px, py, '+-', lw=0.5, label=f'{omega0}')
plt.legend()
plt.tight_layout()
plt.show()
