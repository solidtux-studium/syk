module syk
    use :: iso_c_binding
    use :: ieee_arithmetic

    implicit none

    complex(C_DOUBLE), parameter :: IM = (0.0, 1.0)
    complex(C_DOUBLE), parameter :: PI = 4.0*atan(1.0)
    logical(C_BOOL), parameter :: FALSE = logical(.false., kind=C_BOOL)
    logical(C_BOOL), parameter :: TRUE = logical(.true., kind=C_BOOL)
    real(C_DOUBLE), parameter :: mix = 0.02
    integer(C_INT32_t), parameter :: FFT_LIMIT = 2

contains
    real(C_DOUBLE) function phonon(eta, beta, g, omega0, eps, n, maxiter) bind(C, name="syk_phonon")
        real(C_DOUBLE), intent(in) :: eta, beta, g, omega0, eps
        integer(C_INT32_t), intent(in) :: n, maxiter

        complex(C_DOUBLE), allocatable :: &
            g_tau(:), g_omega(:), &
            f_tau(:), f_omega(:), &
            d_tau(:), d_omega(:), &
            sigma_tau(:), sigma_omega(:), &
            phi_tau(:), phi_omega(:), &
            pi_tau(:), pi_omega(:), &
            old(:,:)
        real(C_DOUBLE) :: dist, om
        integer :: i, j

        ! Allocate memory and initialize variables
        allocate( &
            g_tau(n), g_omega(n), &
            f_tau(n), f_omega(n), &
            d_tau(n), d_omega(n), &
            sigma_tau(n), sigma_omega(n), &
            phi_tau(n), phi_omega(n), &
            pi_tau(n), pi_omega(n), &
            old(3, n))
        g_tau = 0
        g_omega = 0
        f_tau = 0
        f_omega = 0
        d_tau = 0
        d_omega = 0
        sigma_tau = 0
        sigma_omega = 0
        phi_tau = 0
        phi_omega = 0
        pi_tau = 0
        pi_omega = 0
        dist = ieee_value(dist, ieee_positive_inf)
        old = 0

        ! Iterate until converged
        do j = 1,maxiter
            dist = 0

            old(1,:) = g_omega
            old(2,:) = f_omega
            old(3,:) = d_omega
            ! TODO vectorize
            do i = 1,n
                om = omega(i, n, beta, FALSE)
                g_omega(i) = (sigma_omega(n-i+1) - IM * om) / &
                    (abs(phi_omega(i))**2 + (om + IM * sigma_omega(n-i+1)) * (om + IM * sigma_omega(i)))
                f_omega(i) = -phi_omega(i) / &
                    (abs(phi_omega(i))**2 + (om + IM * sigma_omega(n-i+1)) * (om + IM * sigma_omega(i)))
            enddo
            d_omega = 1./(omega0**2 - pi_omega)
            g_omega = mix*g_omega + (1-mix)*old(1,:)
            f_omega = mix*f_omega + (1-mix)*old(2,:)
            d_omega = mix*d_omega + (1-mix)*old(3,:)
            old(1,:) = old(1,:) - g_omega
            old(2,:) = old(2,:) - f_omega
            old(3,:) = old(3,:) - d_omega
            dist = dist + sum(abs(old))/(3*real(n))

            call fft(g_omega, g_tau, FALSE, TRUE, n, beta)
            call fft(f_omega, f_tau, FALSE, TRUE, n, beta)
            call fft(d_omega, d_tau, TRUE, TRUE, n, beta)

            old(1,:) = sigma_tau
            old(2,:) = phi_tau
            old(3,:) = pi_tau
            sigma_tau = g**2 * g_tau * d_tau
            phi_tau = (2 * eta - 1) * g**2 * f_tau * d_tau
            pi_tau = -2 * g**2 * (abs(g_tau)**2 + (2 * eta - 1) * abs(f_tau)**2)
            sigma_tau = mix*sigma_tau + (1-mix)*old(1,:)
            phi_tau = mix*phi_tau + (1-mix)*old(1,:)
            pi_tau = mix*pi_tau + (1-mix)*old(1,:)
            old(1,:) = old(1,:) - sigma_tau
            old(2,:) = old(2,:) - phi_tau
            old(3,:) = old(3,:) - pi_tau
            dist = dist + sum(abs(old))/(3*real(n))

            call fft(sigma_tau, sigma_omega, FALSE, FALSE, n, beta)
            call fft(phi_tau, phi_omega, FALSE, FALSE, n, beta)
            call fft(pi_tau, pi_omega, TRUE, FALSE, n, beta)

            if (dist < eps) then
                exit
            endif
        enddo

        phonon = sum(abs(phi_omega)**2)/real(n)

        ! Clean up
        deallocate( &
            g_tau, g_omega, &
            f_tau, f_omega, &
            d_tau, d_omega, &
            sigma_tau, sigma_omega, &
            phi_tau, phi_omega, &
            pi_tau, pi_omega, &
            old)
    end function phonon

    recursive subroutine fft(x, y, bosonic, inverse, n, beta) bind(C, name="syk_fft")
        complex(C_DOUBLE), dimension(n), intent(in) :: x
        complex(C_DOUBLE), dimension(n), intent(out) :: y
        logical(C_BOOL), intent(in) :: bosonic, inverse
        integer(C_INT32_t), intent(in) :: n
        real(C_DOUBLE), intent(in) :: beta

        integer :: i, j, a

        print *, x
        print *, y
        print *, bosonic, inverse, n, beta

        if (bosonic) then
            a = 0
        else
            a = 1
        endif

        y = 0

        if ((n < FFT_LIMIT) .or. (mod(n, 2) == 1)) then
            if (inverse) then
                do i = 1,n
                    do j = 1,n
                        y(i) = y(i) + exp(-IM * &
                            tau(i, n, beta) * &
                            omega(j, n, beta, bosonic)) * x(j)
                    enddo
                enddo
                y = y / beta
            else
                do i = 1,n
                    do j = 1,n
                        y(i) = y(i) + exp(IM * &
                            tau(j, n, beta) * &
                            omega(i, n, beta, bosonic)) * x(j)
                    enddo
                enddo
                y = y * beta / real(n)
            endif
        else
            call fft(x(1::2), y(1::2), bosonic, inverse, n/2, beta)
            call fft(x(2::2), y(2::2), bosonic, inverse, n/2, beta)
            if (inverse) then
                y(2::2) = y(2::2) * (/ (exp(2*IM*beta*omega(i, n, beta, bosonic)/real(n)), i = 1,n) /)
            else
                y(2::2) = y(2::2) * (/ (exp(-2*PI*IM*tau(i, n, beta)/beta), i = 1,n) /)
            endif
        endif
    end subroutine fft

    function tau(i, n, beta) bind(C, name="syk_tau")
        integer(C_INT32_t), intent(in) :: i, n
        real(C_DOUBLE), intent(in) :: beta
        real(C_DOUBLE) :: tau

        tau = beta * (2. * real(i - 1) / real(n) - 1.)
    end function tau

    function omega(i, n, beta, bosonic) bind(C, name="syk_omega")
        integer(C_INT32_t), intent(in) :: i, n
        real(C_DOUBLE), intent(in) :: beta
        logical(C_BOOL), intent(in) :: bosonic
        real(C_DOUBLE) :: omega

        if (bosonic) then
            omega = real(2 * i - n) * PI / beta
        else
            omega = real(2 * i - n - 1) * PI / beta
        endif
    end function omega
endmodule syk
